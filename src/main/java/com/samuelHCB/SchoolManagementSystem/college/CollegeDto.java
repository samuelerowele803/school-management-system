package com.samuelHCB.SchoolManagementSystem.college;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
@Builder
public class CollegeDto implements Serializable {
    private final String name;
    private final String alias;
    private final Integer admittingCapacity;
    private final Integer departmentSize;
    private final Date yearCreated;


}
