package com.samuelHCB.SchoolManagementSystem.college;

import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeNotFoundException;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.course.CourseRepository;
import com.samuelHCB.SchoolManagementSystem.course.courseExceptions.CourseNotFoundException;
import com.samuelHCB.SchoolManagementSystem.dean.Deans;
import com.samuelHCB.SchoolManagementSystem.dean.DeansRepository;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.department.DepartmentRepository;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentAlreadyExist;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentNotFoundException;
import com.samuelHCB.SchoolManagementSystem.hod.Hods;
import com.samuelHCB.SchoolManagementSystem.hod.HodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class CollegeServiceImp implements CollegeService{
    @Autowired
    private final CollegeRepository collegeRepository;
    @Autowired
    private DeansRepository deansRepository;
    @Autowired
    private HodsRepository hodsRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private CourseRepository courseRepository;



    @Override
    public College addCollege(CollegeDto collegeDto) {
        College college = new College();
        College collegeCheck = collegeRepository.findByName(collegeDto.getName());

        if (collegeCheck!=null){
            throw new CollegeAlreadyExistException("This college already exist");
        }else {
           college.setName(collegeDto.getName());
           college.setAdmittingCapacity(collegeDto.getAdmittingCapacity());
           college.setAlias(collegeDto.getAlias());
           college.setDepartmentSize(collegeDto.getDepartmentSize());
            return collegeRepository.save(college);

        }
    }

    @Override
    public List<College> getAllCollege() {
        List<College> collegeList = collegeRepository.findAll();
        return collegeList;
    }
    @Override
    public College getCollegeByName(String name) {
        College college = collegeRepository.findByName(name);
        if (college!=null){
           return college;
        }else {
            throw  new CollegeNotFoundException("This college doest exist:");
        }
    }

    @Override
    public College deleteByName(String collegeName) {
        College college = collegeRepository.findByName(collegeName);
        collegeRepository.delete(college);

        return college;
    }


    public College addCourseToCollege(String collegeName, String courseName) {
        College college = collegeRepository.findByName(collegeName);
        List<Course> course = courseRepository.findAll().stream().filter(course1 -> course1.getCourseName()
                .equalsIgnoreCase(courseName)).collect(Collectors.toList());
        if (college!=null){
            college.setCourses(course);
            return  collegeRepository.save(college);

        }else {
            throw new CollegeNotFoundException("College with not found");
        }


    }

    @Override
    public void remove(String collegeName) {
        College college = collegeRepository.findByName(collegeName);
        if (college == null) {
            throw new CollegeNotFoundException("This college doesn't exist" + collegeName);
        } else {
            collegeRepository.deleteByName(collegeName);
        }
    }


    @Override
    public College update(String collegeName, CollegeDto collegeDto) {
        College college = collegeRepository.findByName(collegeName);
        if (college == null){
            throw new CollegeNotFoundException("This college doesn't exist" + collegeName);
        }else {
            college.setName(collegeDto.getName());
            college.setAlias(collegeDto.getAlias());
            college.setYearCreated(collegeDto.getYearCreated());
            college.setDepartmentSize(collegeDto.getDepartmentSize());
            college.setAdmittingCapacity(collegeDto.getAdmittingCapacity());
            return collegeRepository.save(college);
        }
    }

    @Override
    public College correctData(String collegeName, CollegeDto collegeDto) {
        College college = collegeRepository.findByName(collegeName);
        if (college == null) {
            throw new CollegeNotFoundException("This college doesn't exist" + collegeName);
        }
        if (collegeDto.getName()!=null){
            college.setName(collegeDto.getName());
        }if (collegeDto.getAlias()!=null){
            college.setAlias(collegeDto.getAlias());
        }if (collegeDto.getDepartmentSize()!=null){
            college.setDepartmentSize(collegeDto.getDepartmentSize());
        }if ((collegeDto.getAdmittingCapacity()!=null)){
            college.setAdmittingCapacity(collegeDto.getAdmittingCapacity());
        }if (collegeDto.getYearCreated()!=null){
            college.setYearCreated(collegeDto.getYearCreated());
        }

        return collegeRepository.save(college);
    }


    @Override
    public College addDeanToCollege(String collegeName, String deanName) {
       College college = collegeRepository.findByName(collegeName);
       Deans deans = deansRepository.findByFullName(deanName);
        if (college!=null){
            college.setDeans(deans);
            return collegeRepository.save(college);
        }else {
            throw new CollegeNotFoundException("College with not found");
        }

    }


    @Override
    public College addHodsToCollege(String collegeName,List<String> hodName) {
        College college = collegeRepository.findByName(collegeName);
        List<Hods> hods = hodsRepository.findAllByFullNameIn(hodName);
        if (college!=null){
            college.setHods( hods);
            return  collegeRepository.save(college);
        }else {
            throw new CollegeNotFoundException("College with not found");
        }

    }

    @Override
    public College addDepartmentsCollege(String collegeName, List<String> departmentName) {
        College college = collegeRepository.findByName(collegeName);
        List<Department> departments = departmentRepository.findAllByNameIn(departmentName);
        List<Department>  departmentInCollege = college.getDepartments();
        List<String> departmentInCollegeNames = new ArrayList<>();


        for (Department d:departmentInCollege) {

            departmentInCollegeNames.add(d.getName());

            }

        boolean departmentExist= new HashSet<>(departmentInCollegeNames).containsAll(departmentName);

        if (college == null) {

            throw new CollegeNotFoundException("College not found");

        } else if(departments.size()==0) {
            throw new DepartmentNotFoundException("no departments found");


        } else if (departmentExist) {
            throw new DepartmentAlreadyExist("this departments:" + departmentExist+ "already exist");

        }else {
            college.setDepartments( departments);
            return collegeRepository.save(college);

        }


}
