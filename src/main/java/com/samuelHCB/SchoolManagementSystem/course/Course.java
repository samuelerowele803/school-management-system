package com.samuelHCB.SchoolManagementSystem.course;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samuelHCB.SchoolManagementSystem.college.College;
import com.samuelHCB.SchoolManagementSystem.dean.Deans;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.hod.Hods;
import com.samuelHCB.SchoolManagementSystem.lectures.Lectures;
import com.samuelHCB.SchoolManagementSystem.student.Student;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long courseId;


    @Column(name = "course_code")
    private String courseCode;

    @Column(name = "name")
    private String courseName;

    @Column(name = "title")
    private String title;

    @Column(name = "level", nullable = false)
    private Long level;

    @Column(name = "duration")
    private String duration;


    @ManyToOne
    @JoinColumn(name = "college_id")
    private College college;



    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Department department;


    @ManyToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Student> students ;


   @JsonIgnore
    @ManyToMany(mappedBy = "courses", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH},
            fetch = FetchType.LAZY)
    private List<Lectures> lectures;





}