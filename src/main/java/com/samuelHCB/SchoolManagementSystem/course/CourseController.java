package com.samuelHCB.SchoolManagementSystem.course;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
@Slf4j
public class CourseController {

    private final CourseService courseService;

    @PostMapping("/addCourse")
    public ResponseEntity<String> addCourse( @RequestBody CourseDto courseDto){
        log.info("Adding course", courseDto.getName());
        courseService.addCourse(courseDto);
        return new ResponseEntity<>("Course added successfully", HttpStatus.CREATED);

    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Course>> getAll(){
        log.info("Getting all course");
        return new ResponseEntity<>(courseService.getAllCourse(),HttpStatus.CREATED);
    }

    @GetMapping("/getCourse")
    public ResponseEntity<Course> getCourseByName(@RequestParam("courseName")String courseName){
        return new ResponseEntity<>(courseService.getCourseByName(courseName),HttpStatus.FOUND);
    }


}
