package com.samuelHCB.SchoolManagementSystem.course;

import com.samuelHCB.SchoolManagementSystem.course.courseExceptions.CourseAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.course.courseExceptions.CourseNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CourseServiceImp implements CourseService{
    private final CourseRepository courseRepository;

    @Override
    public Course addCourse(CourseDto courseDto) {
        Course course = new Course();
        Course courseCheck =courseRepository.findByCourseName(courseDto.getName());
        if (courseCheck!=null){
            throw  new CourseAlreadyExistException(" Course with same name already exist");
        }else {
           course.setCourseCode(courseDto.getCourseCode());
           course.setDuration(courseDto.getDuration());
           course.setCourseName(courseDto.getName());
           course.setLevel(courseDto.getLevel());
           course.setTitle(courseDto.getTitle());
           return courseRepository.save(course);

        }
    }

    @Override
    public List<Course> getAllCourse() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourseByName(String name) {
        Course course = courseRepository.findByCourseName(name);
        if (course!=null) {
            return course;
        }else {
            throw new CourseNotFoundException("Course not found");
        }

    }
}
