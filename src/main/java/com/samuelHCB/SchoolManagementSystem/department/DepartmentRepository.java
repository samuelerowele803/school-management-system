package com.samuelHCB.SchoolManagementSystem.department;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
    Department findByName(String departmentName);
    List<Department> findAllByNameIn(List<String> departmentName);

}