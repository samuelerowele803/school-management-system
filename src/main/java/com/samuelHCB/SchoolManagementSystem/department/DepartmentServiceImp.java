package com.samuelHCB.SchoolManagementSystem.department;

import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.course.CourseRepository;
import com.samuelHCB.SchoolManagementSystem.course.courseExceptions.CourseNotFoundException;
import com.samuelHCB.SchoolManagementSystem.dean.DeansRepository;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentAlreadyExist;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentNotFoundException;
import com.samuelHCB.SchoolManagementSystem.hod.Hods;
import com.samuelHCB.SchoolManagementSystem.hod.HodsRepository;
import com.samuelHCB.SchoolManagementSystem.lectures.Lectures;
import com.samuelHCB.SchoolManagementSystem.lectures.LecturesRepository;
import com.samuelHCB.SchoolManagementSystem.lectures.lecturerException.LecturerAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.lectures.lecturerException.LecturerNotFoundException;
import com.samuelHCB.SchoolManagementSystem.student.Student;
import com.samuelHCB.SchoolManagementSystem.student.StudentRepository;
import com.samuelHCB.SchoolManagementSystem.student.studentExceptions.StudentAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.student.studentExceptions.StudentNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImp implements DepartmentService{

    private final DepartmentRepository departmentRepository;
    @Autowired
    private final StudentRepository studentRepository;
    private final LecturesRepository lecturesRepository;
    @Autowired
    private DeansRepository deansRepository;
    private final HodsRepository hodsRepository;
    @Autowired
    private final CourseRepository courseRepository;

    @Override
    public Department addDepartment(DepartmentDto departmentDto) {
        Department department = new Department();
        Department departmentCheck = departmentRepository.findByName(departmentDto.getName());
        if (departmentCheck!=null){
            throw new DepartmentAlreadyExist("This department already exist");
        }else {
            department.setName(departmentDto.getName());
            department.setAlias(departmentDto.getAlias());
            department.setAdmittingCapacity(departmentDto.getAdmittingCapacity());
            department.setCourseOfStudy(department.getCourseOfStudy());
            department.setCourseDuration(departmentDto.getCourseDuration());
            department.setYearCreated(departmentDto.getYearCreated());

            return  departmentRepository.save(department);
        }

    }

    @Override
    public List<Department> getAll() {
        List<Department> departments = departmentRepository.findAll();

        return departments;
    }

    @Override
    public Department getByName(String departmentName) {
        Department department = departmentRepository.findByName(departmentName);
        if (department==null) {
            throw new DepartmentNotFoundException("department no found");
        }
            return department;



    }

    @Override
    public Department addStudent(String departmentName,String firstName, String surName) {
        Department department = departmentRepository.findByName(departmentName);
        if (department==null){
            throw new DepartmentNotFoundException("This department does not exist");
        }
        List<Student> studentList = studentRepository.findAll();
        List<Student> student = studentList.stream().
                filter(stu -> stu.getFirstName().equalsIgnoreCase(firstName) &&
                        stu.getSurName().equalsIgnoreCase(surName)).collect(Collectors.toList());

        List<Student> studentsInDepartment = department.getStudents();
        boolean studentAlreadyExist =false;
        for (Student s: studentsInDepartment) {
           if (s.getFirstName().equalsIgnoreCase(firstName)&& s.getSurName().equalsIgnoreCase(surName)){
               studentAlreadyExist=true;
           }
        }

        if (department==null){
            throw new DepartmentNotFoundException("This department does not exist");
        }else if(student==null) {
                throw new StudentNotFoundException("Student with  such name Does not exist");
        }else if (studentAlreadyExist) {
               throw new StudentAlreadyExistException("This student already exist in this department");

        }else {
            department.setStudents(student);
           return departmentRepository.save(department);

        }
    }

    @Override
    public Department addHod(String departmentName,String hodName) {
        Department department = departmentRepository.findByName(departmentName);

        Hods hods = hodsRepository.findByFullName(hodName);



        return null;
    }

    @Override
    public Department addCourse(String departmentName, List<Course> courses) {
        Department department = departmentRepository.findByName(departmentName);
        if (department==null){
            throw  new DepartmentNotFoundException("department not found");
        }
        List<Course> courseList = department.getCourses();
        if (courseList.containsAll(courses)) {
            throw new CollegeAlreadyExistException("course already in this department");
        }

        department.setCourses(courses);
        departmentRepository.save(department);
        return department;
    }


    @Override
    public Department addLecturer(String departmentName,String lectureName) {
        Department department = departmentRepository.findByName(departmentName);
        List<Lectures> lecture = lecturesRepository.findAll().stream().filter(lectures -> lectures.getFullName()
                .equalsIgnoreCase(lectureName)).collect(Collectors.toList());
        boolean lectureAlreadyExist=false;
        List<Lectures> lecturesListInDepartment =department.getLectures();
        for (Lectures l:lecturesListInDepartment) {
            if (l.getFullName().equalsIgnoreCase(lectureName)){
                lectureAlreadyExist=true;
            }

        }
        if (department.equals(null)){
            throw new DepartmentNotFoundException("Department not found");
        } else if (lecture.equals(null)) {
            throw  new LecturerNotFoundException("THis lecturer is not present");
        } else if (lectureAlreadyExist) {
            throw  new LecturerAlreadyExistException("This lecture already exist ");

        } else {
            department.setLectures(lecture);
            return departmentRepository.save(department);
        }
    }
}
