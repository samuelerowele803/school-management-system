package com.samuelHCB.SchoolManagementSystem.department.departmentExceptions;

public abstract class DepartmentException extends RuntimeException {
    public DepartmentException(String message) {
        super(message);
    }
}
