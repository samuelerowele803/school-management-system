package com.samuelHCB.SchoolManagementSystem.department.departmentExceptions;

import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeAlreadyExistException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class DepartmentExceptionHandler {

    @ExceptionHandler(DepartmentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<DepartmentExceptionResponse> departmentNotFoundExceptionResponse(DepartmentNotFoundException ex) {
        DepartmentExceptionResponse departmentExceptionResponse = new DepartmentExceptionResponse();
        departmentExceptionResponse.setError(HttpStatus.NOT_FOUND.name());
        departmentExceptionResponse.setMessage(ex.getMessage());
        departmentExceptionResponse.setStatusCode(HttpStatus.NOT_FOUND.value());
        departmentExceptionResponse.setHttpStatus(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(departmentExceptionResponse, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler(DepartmentAlreadyExist.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<DepartmentExceptionResponse> departmentAlreadyExistResponse(CollegeAlreadyExistException ex) {
        DepartmentExceptionResponse departmentExceptionResponse = new DepartmentExceptionResponse();
        departmentExceptionResponse.setError(HttpStatus.CONFLICT.name());
        departmentExceptionResponse.setMessage(ex.getMessage());
        departmentExceptionResponse.setStatusCode(HttpStatus.CONFLICT.value());
        departmentExceptionResponse.setHttpStatus(HttpStatus.CONFLICT);
        return new ResponseEntity<>(departmentExceptionResponse, HttpStatus.CONFLICT);

    }
}
