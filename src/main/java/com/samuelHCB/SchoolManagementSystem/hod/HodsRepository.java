package com.samuelHCB.SchoolManagementSystem.hod;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface HodsRepository extends JpaRepository<Hods, Long> {




    Hods findByFullName(String hodName);

    List<Hods> findAllByFullNameIn(List<String> hodName);
}