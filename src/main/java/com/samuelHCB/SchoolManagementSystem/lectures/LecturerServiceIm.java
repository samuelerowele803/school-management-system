package com.samuelHCB.SchoolManagementSystem.lectures;


import com.samuelHCB.SchoolManagementSystem.college.College;
import com.samuelHCB.SchoolManagementSystem.college.CollegeRepository;
import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeNotFoundException;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.course.CourseRepository;
import com.samuelHCB.SchoolManagementSystem.course.courseExceptions.CourseNotFoundException;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.department.DepartmentRepository;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentNotFoundException;
import com.samuelHCB.SchoolManagementSystem.lectures.lecturerException.LecturerAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.lectures.lecturerException.LecturerNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LecturerServiceIm implements LecturesService{
    @Autowired
    public final LecturesRepository lecturesRepository;
    @Autowired
    private final CourseRepository courseRepository;
    @Autowired
    private final CollegeRepository collegeRepository;
    @Autowired
    private  final DepartmentRepository departmentRepository;

    @Override
    public Lectures addLectures(String collegeName,String departmentName,LecturesDto lecturesDto) {
        Lectures lectures = new Lectures();
        Department department = departmentRepository.findByName(departmentName);
        College college = collegeRepository.findByName(collegeName);
        Lectures lecturesCheck = lecturesRepository.findByFullName(lecturesDto.getFullName());
        if (lecturesCheck!=null){
            throw new LecturerAlreadyExistException("Lecturer already exist");
        } else if (college==null) {throw  new CollegeNotFoundException("this college is not found");

        } else if (department== null) {throw new DepartmentNotFoundException("this department does not exist");
        } else {
            lectures.setFullName(lecturesDto.getFullName());
            lectures.setEmail(lecturesDto.getEmail());
            lectures.setGender(lecturesDto.getGender());
            lectures.setLocalGovernmentArea(lecturesDto.getLocalGovernmentArea());
            lectures.setMobileNumber(lecturesDto.getMobileNumber());
            lectures.setNationality(lecturesDto.getNationality());
            lectures.setStateOfOrigin(lecturesDto.getStateOfOrigin());
            lectures.setYearOfInception(lecturesDto.getYearOfInception());
            lectures.setCollege(college);
            lectures.setDepartment(department);
            return lecturesRepository.save(lectures);

        }

    }

    @Override
    public Lectures assignCourses(String lecturerName, String departmentName,String courseName) {
        Lectures lecturer = lecturesRepository.findByFullName(lecturerName);
        if (lecturer==null) {
            throw new LecturerNotFoundException("THIS LECTURER IS NOT AVAILABLE");
        }
        Department lecturesDepartment = departmentRepository.findByName(departmentName);
        if (lecturesDepartment==null) {
            throw  new DepartmentNotFoundException("THIS DEPARTMENT IS NOT AVAILABLE");}
        List<Course> lecturesDepartmentCourses = lecturesDepartment.getCourses();
        List<Course> course = courseRepository.findAll().stream().filter(c->c.getCourseName().equalsIgnoreCase(courseName)).
                collect(Collectors.toList());
        if (course==null){
            throw  new CourseNotFoundException("Course not found");
        }else if (lecturesDepartmentCourses!=null) {
            course.removeAll(lecturesDepartmentCourses);
        }
            lecturer.setCourses(course);
            return lecturesRepository.save(lecturer);


    }

    @Override
    public List<Lectures> getAllLectures() {
        List<Lectures> lectures = lecturesRepository.findAll();
        return lectures;
    }

    @Override
    public Lectures getLectureByName(String fullName) {
       Lectures lectures = lecturesRepository.findByFullName(fullName);
       if (lectures==null){
           throw new LecturerNotFoundException("Lecturer not found");
       }else {
           return lectures;
       }

    }
}
