package com.samuelHCB.SchoolManagementSystem.lectures;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LecturesRepository extends JpaRepository<Lectures, Long> {
    Lectures findByFullName(String fullName);
}