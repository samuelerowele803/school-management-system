package com.samuelHCB.SchoolManagementSystem.lectures;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LecturesService {
    Lectures addLectures(String collegeName,String departmentName,LecturesDto lecturesDto);
    Lectures assignCourses(String lecturerName, String departmentName, String courses);
    List<Lectures> getAllLectures();
    Lectures getLectureByName(String fullName);
}
