package com.samuelHCB.SchoolManagementSystem.lectures.lecturerException;

public class LecturerException extends RuntimeException{
    public LecturerException(String message) {
        super(message);
    }
}
