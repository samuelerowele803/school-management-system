package com.samuelHCB.SchoolManagementSystem.student;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
@Slf4j
public class StudentController {
    private final StudentService studentService;
    @PostMapping("/addStudent")
    public ResponseEntity<String> addStudent(@RequestBody StudentDto studentDto,
                                             @RequestParam("collegeName")String collegeName,
                                             @RequestParam("departmentName")String departmentName){
        log.info("Adding student", studentDto.getFirstName(),studentDto.getSurName());
        studentService.addStudent(collegeName,departmentName,studentDto);
        return new ResponseEntity<>("student added successfully", HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Student>> getAll (){
        log.info("GETTING ALL STUDENTS");
        return  new ResponseEntity<>(studentService.getAll(),HttpStatus.FOUND);
    }

    @PostMapping("/assignCollege")
    public ResponseEntity<String> assignCollege(@RequestParam("collegeName")String collegeName,
                                                @RequestParam("firstName")String firstName,
                                                @RequestParam("surname")String surname){
        log.info("Assigning college to student",collegeName);
        studentService.assignCollege(collegeName,firstName,surname);
        return new ResponseEntity<>("college assigned successfully",HttpStatus.CREATED);
    }

    @PostMapping("/courseRegistration")
    public ResponseEntity<Student> courseRegistration(@RequestParam("firstNme")String firstName,
                                                      @RequestParam("surName")String surName,
                                                      @RequestParam("collegeName")String collegeName,
                                                      @RequestParam("departmentName")String departmentName,
                                                      @RequestBody List<String> listOfCourses){
        log.info("Course Registration for:", firstName,surName);
        return new ResponseEntity<>(studentService.courseRegistration(firstName,surName,collegeName,departmentName,listOfCourses),HttpStatus.CREATED);
    }
}
