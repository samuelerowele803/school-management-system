package com.samuelHCB.SchoolManagementSystem.student;

import com.samuelHCB.SchoolManagementSystem.college.College;
import com.samuelHCB.SchoolManagementSystem.college.CollegeRepository;
import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeNotFoundException;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.course.CourseRepository;
import com.samuelHCB.SchoolManagementSystem.course.courseExceptions.CourseNotFoundException;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.department.DepartmentRepository;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentNotFoundException;
import com.samuelHCB.SchoolManagementSystem.student.studentExceptions.StudentAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.student.studentExceptions.StudentNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImp implements StudentService{
   @Autowired
    private  StudentRepository studentRepository;
   @Autowired
   private final CollegeRepository collegeRepository;
   @Autowired
   private  final DepartmentRepository departmentRepository;
   @Autowired
   CourseRepository courseRepository;

    @Override
    public Student addStudent(String collegeName,String departmentName,StudentDto studentDto) {
        Student student = new Student();
        Student studentCheck = studentRepository.findByFirstNameAndSurName(studentDto.getFirstName(),studentDto.getSurName());
        College college = collegeRepository.findByName(collegeName);
        Department department = departmentRepository.findByName(departmentName);
        if (studentCheck != null){
            throw  new StudentAlreadyExistException("this Student Already Exist");

        } else if (college==null) {throw new CollegeNotFoundException("this college does not exist");

        } else if (department==null) {throw new DepartmentNotFoundException("this department does not exist");

        } else {
            student.setFirstName(studentDto.getFirstName());
            student.setMiddleName(studentDto.getMiddleName());
            student.setSurName(studentDto.getSurName());
            student.setGender(studentDto.getGender());
            student.setLevel(studentDto.getLevel());
            student.setLocalGovernmentArea(studentDto.getLocalGovernmentArea());
            student.setStateOfOrigin(studentDto.getStateOfOrigin());
            student.setMobileNumber(studentDto.getMobileNumber());
            student.setNationality(studentDto.getNationality());
            student.setCollege(college);
            student.setDepartment(department);
            return studentRepository.save(student);
        }
    }

    @Override
    public List<Student> getAll() {
        List<Student> students = studentRepository.findAll();
        return students;
    }

    @Override
    public Student findByFirstNameAndSurName(String firstName, String surName) {
        Student student = studentRepository.findByFirstNameAndSurName(firstName, surName);
        if (student == null) {
            throw new StudentNotFoundException("Student which such name does not exist");
        }

        return student;
    }

    @Override
    public Student assignCollege(String collegeName, String firstName, String surName) {
        College college = collegeRepository.findByName(collegeName);
        Student student = studentRepository.findByFirstNameAndSurName(firstName,surName);
        College studentAlreadyInCollege = student.getCollege();

        if (college.equals(null)){
            throw  new CollegeNotFoundException("college not available");
        } else if (studentAlreadyInCollege !=null) {
            throw  new CollegeAlreadyExistException("This student already have a college assigned");

        }else {
            student.setCollege(college);
            return studentRepository.save(student);
        }

    }

    @Override
    public Student courseRegistration(String Firstname, String surname,String collegeName, String departmentName, List<String> courses) {
        Student student = studentRepository.findByFirstNameAndSurName(Firstname,surname);
        if (student==null){
            throw  new StudentNotFoundException("this student does not exist");
        }
        College college = collegeRepository.findAll().stream().filter(c->c.getName().equalsIgnoreCase(departmentName))
                .findAny().orElseThrow(()-> new CollegeNotFoundException("College not found"));
        Department department = departmentRepository.findAll().stream().filter(d-> d.getName().equalsIgnoreCase(departmentName))
                .findAny().orElseThrow(()-> new DepartmentNotFoundException("this department not available"));
        List<Course> course = courseRepository.findByCourseNameIn(courses);

        List<Course> courseListInDepartment =department.getCourses();
        List<String> courseListInDepartmentNames = new ArrayList<>();
        for (Course c:courseListInDepartment) {
            courseListInDepartmentNames.add(c.getCourseName());

        }
        boolean courseAvailable = new HashSet<>(courseListInDepartmentNames).containsAll(courses);
        if (courseAvailable==false){throw new CollegeNotFoundException("these courses is not available for your department");

        }else {
            student.setCourses(course);
            return studentRepository.save(student);

        }

    }
}
