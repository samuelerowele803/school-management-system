package com.samuelHCB.SchoolManagementSystem.student.studentExceptions;

public class StudentAlreadyExistException extends StudentException{
    public StudentAlreadyExistException(String message) {
        super(message);
    }
}
